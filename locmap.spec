%if 0%{?rhel} >= 8
%global python_sitelib %python3_sitelib
%global python_install %py3_install
%global python_build %py3_build
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires: python3-ldap
Requires: python3-netaddr
Requires: python3-PyYAML
Requires: python3-setuptools
%else
%global python_sitelib %python2_sitelib
%global python_install %py2_install
%global python_build %py2_build
BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires: python-ldap
Requires: python-netaddr
Requires: PyYAML
Requires: python-setuptools
%endif
BuildRequires: pandoc

Name:       locmap
Version:    2.0.35
Release:    1%{?dist}
Summary:    Local Configuration with Masterless Puppet
Group:      CERN/Utilities
License:    BSD
URL:        http://linux.cern.ch
Source0:    https://gitlab.cern.ch/linuxsupport/locmap/%{name}-%{version}.tgz

BuildArch:      noarch

Requires: krb5-workstation
Requires: useraddcern
Requires: iproute
Requires: bind-utils

Requires: puppet-agent >= 1.9
Requires: locmap-plugin-puppet-facts
Requires: locmap-plugin-xldap

# Default modules
Requires: puppet-afs
Requires: puppet-cernbox
Requires: puppet-concat
Requires: puppet-cvmfs
Requires: puppet-eosclient
Requires: puppet-firewalld
Requires: puppet-inifile
Requires: puppet-kerberos
Requires: puppet-lpadmin
Requires: puppet-mailalias_core
Requires: puppet-ssh
Requires: puppet-stdlib
Requires: puppet-sudo
Requires: puppet-systemd
Requires: puppet-zoom

%if 0%{?rhel} == 7
# to be moved above to default modules, once a working version exists for C8
# these are only for 7
Requires: puppet-ntp
Requires: puppet-nscd
Requires: puppet-sendmail
# Only on 7 and 8, not needed on 9+ - provides facts.os.distro etc
Requires: redhat-lsb-core
%endif

%if 0%{?rhel} == 8
# Only on 7 and 8, not needed on 9+ - provides facts.os.distro etc
Requires: redhat-lsb-core
%endif

%if 0%{?rhel} >= 8
# locmap-release is installed to provide an upgrade path
# If 'locmap' is installed via a kickstart, the release file may not have been
# installed.
Requires: locmap-release
Requires: puppet-cernphone
Requires: puppet-chrony
Requires: puppet-postfix
Requires: puppet-resolved
Requires: puppet-alternatives
Requires: puppet-augeasproviders_core
Requires: puppet-augeasproviders_pam
%endif

%description
Local Configuration with Masterless Puppet and its plugins

%package plugin-xldap
Summary:        locmap input plugin / CERN xldap
Group:          CERN/Utilities

Requires:       locmap

%description plugin-xldap
locmap input plugin / CERN xldap

%package plugin-puppet-facts
Summary:        locmap input plugin / Puppet facts
Group:          CERN/Utilities

Requires:       locmap

%description plugin-puppet-facts
locmap input plugin / Puppet facts

%prep
%setup -q

%build
%python_build
pandoc -s -t man man/%{name}.md > %{name}.1

%install
%{__rm} -rf %{buildroot}

%python_install

rm -rf %{buildroot}/%{python_sitelib}/tests

install -dm 755 %{buildroot}%{_sysconfdir}/%{name}
install -dm 755 %{buildroot}%{_sysconfdir}/%{name}/enabled
install -dm 755 %{buildroot}%{_sysconfdir}/%{name}/code/environments/production/hieradata
install -dm 755 %{buildroot}%{_sysconfdir}/%{name}/code/environments/production/hieradata/modules
install -dm 755 %{buildroot}%{_sysconfdir}/%{name}/code/environments/production/hieradata/users
install -dm 755 %{buildroot}/%{_datadir}/man/man1
install -dm 755 %{buildroot}/%{_datadir}/puppet/modules

install -m 0644 etc/site.pp  %{buildroot}%{_sysconfdir}/%{name}/site.pp
install -m 0644 etc/locmap.conf  %{buildroot}%{_sysconfdir}/%{name}/locmap.conf
install -m 0644 etc/hiera.yaml  %{buildroot}%{_sysconfdir}/%{name}/hiera.yaml

install -m 0644 locmap.1 %{buildroot}/%{_datadir}/man/man1/

%files plugin-xldap
%{python_sitelib}/%{name}/input_plugins/xldap*

%files plugin-puppet-facts
%{python_sitelib}/%{name}/input_plugins/facts*

%files
%defattr(-,root,root)
%{_bindir}/*
%dir %{_sysconfdir}/locmap/enabled
%dir %{_sysconfdir}/locmap/code/environments/production/hieradata/modules
%dir %{_sysconfdir}/locmap/code/environments/production/hieradata/users
%dir %{_datadir}/puppet/modules
%config(noreplace) %{_sysconfdir}/locmap/locmap.conf
%{_mandir}/man1/locmap.1.gz
%{_sysconfdir}/locmap/site.pp
%{_sysconfdir}/locmap/hiera.yaml
%{python_sitelib}/%{name}*
%exclude %{python_sitelib}/%{name}/input_plugins/facts*
%exclude %{python_sitelib}/%{name}/input_plugins/xldap*
%exclude %{python_sitelib}/tests/*
%doc LICENSE README.rst AUTHORS.rst CONTRIBUTING.rst

%changelog
* Mon Sep 30 2024 Ben Morrice <ben.morrice@cern.ch> - 2.0.35-1
- Allow the user to control if locmap should automatically reconfigure modules
  during package upgrades

* Fri May 03 2024 Ben Morrice <ben.morrice@cern.ch> - 2.0.34-1
- Update man page to reflect reality

* Tue Oct 10 2023 Ben Morrice <ben.morrice@cern.ch> - 2.0.33-3
- remove puppet-nscd 8+, adding puppet-resolved instead

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> - 2.0.33-2
- remove puppet-keytab as a depedency

* Wed Oct 04 2023 Ben Morrice <ben.morrice@cern.ch> - 2.0.33-1
- Suppress logging of stdlib deprecations

* Mon Oct 02 2023 Manuel Guijarro <manuel.guijarro@cern.ch> - 2.0.32-3
- Updated CERN subnets based on what is listed at: https://landb.cern.ch/portal/ip-networks 

* Mon Apr 03 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 2.0.32-1
- remove managedby from the unique_logins

* Thu Mar 16 2023 Ben Morrice <ben.morrice@cern.ch> - 2.0.31-1
- Add message for zoom on 9 about screen sharing

* Mon Mar 13 2023 Ben Morrice <ben.morrice@cern.ch> - 2.0.30-1
- bugfix aarch64 detection logic

* Thu Mar 02 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 2.0.29-1
- Add --disallow_root flag to disable root access of main users in LanDB
- Tweak modules available on aarch64 systems

* Wed Dec 07 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.28-1
- Fix corner case of locmap crash due to bad ldap data

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-7
- Bump release for disttag change

* Thu Dec 01 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-6
- update callhome logic

* Tue Aug 30 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-5
- call useraddcern with '-s'

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-4
- enable cernphone on CS8,CS9 (appimage install)
- enable cernbox on CS9 (appimage install)

* Mon Jul 04 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-3
- enable eosclient on CS9

* Fri Jul 01 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-2
- enable afs on CS9

* Wed May 25 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.27-1
- call useraddcern with --login

* Tue May 03 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.26-3
- add redhat-lsb-core to Requires for CC7 and C8 (not needed on el9+)

* Fri Apr 29 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.26-2
- enable cvmfs for CS9

* Thu Apr 21 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.26-1
- fix checkin routine for RHEL hosts

* Wed Apr 20 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.25-1
- tweak operatingsystem hieradata, allowing version major

* Tue Apr 19 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.24-1
- fix yaml warning on CS9+

* Mon Apr 04 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.23-1
- update hiera configuration to support optionally
  module/operatingsystem, enabling the ability to
  pass configuration specific to an OS (RHEL, etc)

* Thu Feb 03 2022 Ben Morrice <ben.morrice@cern.ch> - 2.0.22-5
- enable postfix on el9

* Mon Nov 22 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.22-4
- remove postfix on el9 for now

* Wed Nov 17 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.22-3
- remove items that are not currently supported on el9

* Tue Nov 16 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.22-2
- clean up spec, build for el9

* Thu Oct 28 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.22-1
- add puppet-zoom

* Mon May 17 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.21-1
- add extra xldap logic to ensure host lookups still work under
  certain conditions

* Thu Apr 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.20-1
- add report home logic

* Tue Mar 09 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.19-1
- fix logic error with enable/disable

* Thu Mar 04 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.18-1
- add 'all' parameter to --enable and --disable arguments

* Wed Mar 03 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.17-1
- fix verbose output on python3
- remove old version logic

* Wed Mar 03 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.16-1
- remove node defition in site.pp, in preparation for puppet-agent 6

* Fri Feb 19 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.15-1
- add mailalias_core module in preparation for puppet6
- enable nscd for C8

* Mon Feb 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.14-1
- upgrade afs on c7 (systemd dependency)

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.13-1
- enable cernbox on c8

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0.12-1
- enable eosclient on c8

* Wed Sep 02 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.11-1
- don't create local users if they don't have ldap home_dir set

* Wed Mar 25 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-7
- add additional C8 python3 fixes

* Mon Mar 02 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-6
- enable postfix on c8

* Wed Feb 26 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-5
- set Requires locmap-release on C8 to provide an upgrade path

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-4
- set Provides correctly puppet-augeasproviders*

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-3
- enable afs on c8

* Tue Feb 18 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-2
- remove Requires for currently broken modules on C8

* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0.10-1
- add puppet-chrony as a Requires on C8

* Fri Nov 29 2019 Ben Morrice <ben.morrice@cern.ch> - 2.0.9-1
 - add support for el8

* Tue Nov 20 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.8-1
- Remove puppet-gpg dependency

* Tue Nov 20 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.7-1
- fix bug LOS-197 : wrong user list in afs module

* Wed Jul 11 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.5-1
- fix bug LOS-184 : .strip function error.

* Mon Jun 04 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.4-1
- fix KeyError in xldap ssh key fetching.
- fix dn for user's search function

* Fri May 11 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.3-1
- fix Requires for default modules
- fix user message for none existing module
- Add better logging to file

* Fri May 11 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.1-1
- fix none CERN machine
- set -> list for yaml

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-9
- relax puppet-agent requires to >= 1.9

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-8
- Run input plugins only when needed
- Better domain detection if ipv6 local link

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-7
- Fix Requires: for new plugins

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-6
- CentOS 7.5 first version

* Fri Apr 06 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-4
- Split main locmap script and plugins

* Fri Apr 06 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0.0-3
- Version 2.0.0

* Mon Mar 13 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-11
- Fix guessdomain for TN

* Tue Mar 07 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-10
- Fix building missing use case

* Fri Feb 03 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-9
- Fix cn method, for none xldap Linux computers

* Mon Dec 05 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-5
- Fix guessdomain

* Mon Dec 05 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-4
- Obsolete ncm* lcm*

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-1
- Add lcm binary with deprecation message
- Rebuild for 7.3 release

* Mon Nov 21 2016 Aris Boutselis <aris.boutselis@cern.ch> - 1.0-0
- Initial version
