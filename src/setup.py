#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # TODO: put package requirements here
]

setup_requirements = [
    # TODO(alphacc): put setup requirements (distutils extensions, etc.) here
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='locmap',
    version='2.0.0',
    description="Lo(cal) C(onfiguration) with Ma(sterless) P(uppet)",
    long_description=readme + '\n\n' + history,
    author="Thomas Oulevey",
    author_email='thomas.oulevey@cern.ch',
    url='https://github.com/alphacc/locmap',
    #packages=find_packages(include=['locmap']),
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
    license="BSD license",
    zip_safe=False,
    keywords='locmap',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
    entry_points='''
        [console_scripts]
        lcm=locmap.scripts.lcm:main
        locmap=locmap.scripts.cli:main

        [locmap.plugin]
        input_plugin_xldap=locmap.input_plugins.xldap:retrieve_info
        input_plugin_facts=locmap.input_plugins.facts:retrieve_info
    ''',
)
