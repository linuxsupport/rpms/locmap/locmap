% LOCMAP(1)
% Ben Morrice <ben.morrice@cern.ch>
% March 2024

# NAME

locmap - Local Configuration with Masterless Puppet

# DESCRIPTION

Locmap is a tool that uses curated puppet modules to define CERN specific configuration on Linux desktop or server environments

# SYNOPSIS

locmap   --help

locmap   --list

locmap   --enable MODULE

locmap   --disable MODULE

locmap   --configure MODULE

locmap   --verbose

locmap   --info

locmap   --enable_autoreconfigure

locmap   --disable_autoreconfigure

locmap   --disallow_root

locmap   --environment ENVIRONMENT

locmap   --config_dir CONFIG_DIR

# OPTIONS

**\-\-help**

    Shows this help description

**\-\-list**

    List all available puppet modules.
    (located in the /usr/local/share/puppet/modules path)

**\-\-enable MODULE**

    Enable a module from the available puppet module inventory.
    The keyword 'all' can be used to enable all modules.

**\-\-disable MODULE**

    Disable a module from the available puppet module inventory.
    The keyword 'all' can be used to disable all modules.

**\-\-configure MODULE**

    Configures a module (requires it to be first enabled).
    The keyword 'all' can be used to configure all modules.

**\-\-verbose**

    Increase output verbosity

**\-\-info**

    Print the host configuration

**\-\-enable_autoreconfigure**

    Ensures that upgrades of locmap packages can "configure" themselves automatically, providing a mechanism to support automatic reconfiguration of locmap modules
    Locmap modules are only reconfigured if they are "enabled" on the system
    This option writes the "autoreconfigure=True" key value in /etc/locmap/locmap.conf
    Disabled by default

**\-\-disable_autoreconfigure**

    Prevents the automatic configuration of locmap modules during package upgrades
    This option writes the "autoreconfigure=False" key value in /etc/locmap/locmap.conf
    Enabled by default

**\-\-disallow_root**

    Remove root access to main users of LanDB

**\-\-environment ENVIRONMENT**

    Override default puppet environment

**\-\-config_dir CONFIG_DIR**

    Override default config directory

# EXIT STATUS

Locmap will return the exit code of puppet, which has the following return codes:

**0** - The run succeeded with no changes or failures

**1** - The run failed, or was not attempted due to another run already in progres

**2** - The run succeeded, and some resources failed

**4** - The run succeeded, and some resources failed

**6** - The run succeeded, and included both changes and failures


# EXAMPLES

locmap --list

locmap --enable all

locmap --disable eosclient

locmap --configure all

locmap --disallow_root --configure all

# ADDITIONAL INFORMATION

Traditionally (SLC6, CC7) when deploying the "afs" module, locmap would configure users on the system with /afs/cern.ch/home/ directories. The default for current Liunx distributions is to configure a local /home directory.

If you prefer the old behaviour, you can define "LOCMAP_HOMEDIRECTORY_LOCAL=False" in the /etc/sysconfig/locmap-initialsetup and then invoking locmap.

Note - this step would need to be done before invoking 'locmap --configure afs'. If you have already invoked locmap and wish to change from using local /home to afs home you can run:

useraddcern --directory --update USERNAME
