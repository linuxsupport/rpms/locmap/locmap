#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from logging.handlers import TimedRotatingFileHandler


def configure_logging(args, config, level=logging.INFO):
    """Configures application log level based on cmdline arguments"""

    if 'verbose' in args and args.verbose:
        level = logging.DEBUG

    format = '[%(levelname)-8s] %(message)s'
    formatter = logging.Formatter(format)
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(level=level)
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)

    format = '[%(levelname)-8s] %(asctime)s %(message)s'
    formatter = logging.Formatter(format)
    logFilePath = os.path.join(config.log_dir, config.log_file)
    file_handler = TimedRotatingFileHandler(
        filename=logFilePath,
        when='midnight',
        backupCount=10)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)
    root_logger.addHandler(file_handler)
