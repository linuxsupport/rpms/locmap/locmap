import sys
import os

if (sys.version_info > (3, 0)):
  from configparser import ConfigParser, NoOptionError
else:
  from ConfigParser import ConfigParser, NoOptionError

from locmap.params import DEFAULT_CONFIG_FILE, DEFAULT_CONFIG_DIR
from locmap.params import DEFAULT_LOG_DIR, DEFAULT_PUPPET_BIN
from locmap.params import DEFAULT_LOG_FILE
from locmap.params import DEFAULT_MODULES_PATH, DEFAULT_PUPPET_PATH
from locmap.params import DEFAULT_HIERA_PATH, DEFAULT_ENVIRONMENT
from locmap.params import DEFAULT_AUTO_RECONFIGURE


class LocmapConfig(object):
    """
    Class holding locmap config
    """

    def __init__(self, configdir=None, configfile=None, modulespath=None,
                 puppetpath=None, puppetbin=None, logdir=None,
                 puppetsitepp=None, hieradir=None, hieracommon=None,
                 hierapath=None, env=None, logfile=None, autoreconfigure=None):

        self.config_dir = DEFAULT_CONFIG_DIR
        if configdir:
            self.config_dir = configdir

        self.config_file = DEFAULT_CONFIG_FILE
        if configfile:
            self.config_file = configfile

        self.modules_path = DEFAULT_MODULES_PATH
        if modulespath:
            self.modules_path = modulespath

        self.puppet_path = DEFAULT_PUPPET_PATH
        if puppetpath:
            self.puppet_path = puppetpath

        self.hiera_path = DEFAULT_HIERA_PATH
        if hierapath:
            self.hiera_path = hierapath

        self.env = DEFAULT_ENVIRONMENT
        if env:
            self.env = env

        self.puppet_bin = DEFAULT_PUPPET_BIN
        if puppetbin:
            self.puppet_bin = puppetbin

        self.log_dir = DEFAULT_LOG_DIR
        if logdir:
            self.log_dir = logdir

        self.auto_reconfigure = DEFAULT_AUTO_RECONFIGURE
        if autoreconfigure:
            self.auto_reconfigure = autoreconfigure

        self.log_file = DEFAULT_LOG_FILE
        if logfile:
            self.log_file = logfile

    def read_config_and_override_with_pargs(self, pargs):
        self.parser = ConfigParser()
        if pargs.config_dir:
            self.config_dir = pargs.config_dir
        if pargs.environment:
            self.env = pargs.environment

        config_file = os.path.join(self.config_dir, self.config_file)

        try:
            with open(config_file) as f:
                self.parser.readfp(f)
        except IOError:
            sys.stderr.write("Config file (%s) could not be opened.\n" %
                             config_file)
            sys.stderr.write("** Locmap should be run as root.\n")
            sys.exit(10)

        self.config_file = config_file
        try:
            self.modules_path = self.parser.get('general', 'modules_path')
        except NoOptionError:
            pass
        try:
            self.puppet_path = self.parser.get('general', 'puppet_path')
        except NoOptionError:
            pass
        try:
            self.hiera_path = self.parser.get('general', 'hiera_path')
        except NoOptionError:
            pass
        try:
            self.env = self.parser.get('general', 'environment')
        except NoOptionError:
            pass
        try:
            self.log_dir = self.parser.get('general', 'log_dir')
        except NoOptionError:
            pass
        try:
            self.auto_reconfigure = self.parser.get('features', 'autoreconfigure')
        except NoOptionError:
            pass

        self.pargs = vars(pargs)

    def __getattr__(self, key):
        try:
            val = self._get_from_cli(key)
            if val is None:
                val = self._get_from_configfile(key)
            return val
        except:
            raise AttributeError(
                    "'%s' configuration object has no attribute '%s'" %
                    (self.__class__.__name__, key))

    def _get_from_configfile(self, key, section="general"):
        return self.parser.get(section, key)

    def _get_from_cli(self, key):
        return self.pargs.get(key, None)
