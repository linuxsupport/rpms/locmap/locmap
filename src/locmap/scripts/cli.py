#!/usr/bin/env python
# This program is a wrapper around puppet/
# It allows you to configure machine outside of the datacentre
#
# Exit codes:
#     0  all operations succeeded
#     2  invalid module
#    10  system error

import os
import sys
import logging
import argparse
import re
import yaml
import importlib
import platform
import subprocess

if (sys.version_info > (3, 0)):
  import urllib.request
else:
  import urllib2

from locmap.config import LocmapConfig
from locmap.models import Host  # , User
from locmap.logger import configure_logging
from locmap.utils import is_user_admin

def update_conf_value(filename, key, value):
    updated_lines = []
    key_found = False

    with open(filename, 'r') as file:
        for line in file:
            if line.strip().startswith(key + '='):
                updated_lines.append(f"{key}={value}\n")
                key_found = True
            else:
                updated_lines.append(line)

    if not key_found:
        if updated_lines and not updated_lines[-1].endswith('\n'):
            updated_lines.append('\n')
        updated_lines.append(f"{key}={value}\n")

    with open(filename, 'w') as file:
        file.writelines(updated_lines)

def parse_cmdline_args():
    parser = argparse.ArgumentParser(
        description='''
        ** Managing local configuration **,
        ''')
    parser.add_argument('--config_dir', action='store', dest='config_dir',
                        help='Override default config directory')
    parser.add_argument('--environment', action='store', dest='environment',
                        help='Override default puppet environment')
    parser.add_argument('--verbose', action='store_true',
                        help='Increase output verbosity.')
    parser.add_argument('--disallow_root', action='store_true', dest="disallow_root",  
                        help="Remove root access to main users of LanDB", default=False)
    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--info', action='store_true',
                        help='Print gathered information' +
                             ' about the host and exit',
                        default=False)
    action.add_argument('--list', action='store_true',
                        help='List all available puppet modules')
    action.add_argument('--configure', action='store', dest='configure',
                        metavar='MODULE',
                        help='Run specific puppet module against the system \
                              If - all - run all enabled puppet modules')
    action.add_argument('--enable', action='store', dest='enable',
                        metavar='MODULE',
                        help='Enable a specific module, only enabled modules \
                               can run against the system. (e.g --enable ssh)')
    action.add_argument('--disable', action='store', dest='disable',
                        metavar='MODULE',
                        help="Disable a specific module, disabled modules \
                               can't be configured. (e.g --disable ssh)")
    action.add_argument('--enable_autoreconfigure', action='store_true',
                        help='Allow locmap to automatically reconfigure on new package upgrades', default=False)
    action.add_argument('--disable_autoreconfigure', action='store_true',
                        help='Disallow locmap to automatically reconfigure on new package upgrades', default=False)
    args = parser.parse_args()
    return args


def main():
    """Application entrypoint"""

    args = parse_cmdline_args()

    if not is_user_admin():
        print("[ERROR] You should run locmap as root.\n")
        exit(0)

    lconfig = LocmapConfig()
    lconfig.read_config_and_override_with_pargs(args)

    configure_logging(args, lconfig)

    host = Host(args.disallow_root)
    prepare_locmap_config_layout(lconfig)

    fullmodulelist = list_modules(lconfig, organisation="linuxsupport", onlyenabled=False)
    aarch64_forbidden_modules = ['cernbox', 'cernphone', 'zoom' ]
    if 'aarch64' in platform.machine():
        is_aarch64 = True
    else:
        is_aarch64 = False
    # Since locmap is noarch, we are doing this through code rather than in a spec file
    if is_aarch64:
        for rm in aarch64_forbidden_modules:
            fullmodulelist.remove(rm)

    if args.enable_autoreconfigure:
        print('Enabling automatic reconfiguration')
        update_conf_value(lconfig.config_file, 'autoreconfigure', 'True')
        exit(0)
    if args.disable_autoreconfigure:
        print('Disabling automatic reconfiguration')
        update_conf_value(lconfig.config_file, 'autoreconfigure', 'False')
        exit(0)

    if args.enable is not None:
        if "all" in args.enable:
          modulelist =  fullmodulelist
        else:
          modulelist = []
          modulelist.append(args.enable)
        for module in modulelist:
          retcode = modify_module(lconfig, module, 'enable', is_aarch64, aarch64_forbidden_modules)
          if retcode == 0:
            print("[INFO] module %s %sd." % (module, 'enable'))
          else:
            print("[ERROR] module %s does not exist." % (module))
        exit(retcode)
    elif args.disable is not None:
        if "all" in args.disable:
          modulelist = fullmodulelist
        else:
          modulelist = []
          modulelist.append(args.disable)
        for module in modulelist:
          retcode = modify_module(lconfig, module, 'disable', is_aarch64, aarch64_forbidden_modules)
          if retcode == 0:
            print("[INFO] module %s %sd." % (module, 'disable'))
          else:
            print("[ERROR] module %s does not exist." % (module))
        exit(retcode)
    elif args.list:
        print_modules(lconfig, fullmodulelist)
        exit(0)

    run_input_plugins(lconfig, host)

    if args.info:
        print_host_information(lconfig, host)
        exit(0)
    elif args.configure is not None:
        modules = set()
        if args.configure == "all":
            modules = list_modules(lconfig, organisation="linuxsupport")
        else:
            modules.add(args.configure)

        release=open('/etc/redhat-release','r').read().rstrip()
        release_major = re.search('\d+',release).group()
        if re.search('Enterprise', release):
          tag = ('potdrh%s-stable' % release_major)
        elif re.search('Stream', release):
          tag = ('potd%ss-stable' % release_major)
        elif re.search('AlmaLinux', release):
          tag = ('potdal%s-stable' % release_major)
        else:
          tag = ('potd%s-stable' % release_major)
        checkin_modules='/'.join(list(dict.fromkeys(modules)))
        # Note - this url doesn't exist, we just abuse the log infra to gain insight into usage
        url = 'http://linuxsoft.cern.ch/internal/repos/' + tag + '/checkin/' + checkin_modules
        # Report home, we don't care about the http return code
        if (sys.version_info > (3, 0)):
          try:
            urllib.request.urlopen(url)
          except:
            pass
        else:
          try:
            urllib2.urlopen(url)
          except:
            pass

        retcode = configure_module(lconfig, host, modules, is_aarch64, aarch64_forbidden_modules, extra_args="")
        if retcode == 0:
            msg = 'The run succeeded with no changes or failures;'
            msg += ' the system was already in the desired state.'
        elif retcode == 1:
            msg = 'The run failed, or was not attempted'
            msg += ' due to another run already in progress'
        elif retcode == 2:
            msg = 'The run succeeded, and some resources were changed.'
        elif retcode == 4:
            msg = 'The run succeeded, and some resources failed.'
        elif retcode == 6:
            msg = 'The run succeeded, and included both changes and failures.'
        else:
            msg = 'Could not execute puppet.'
        if 'zoom' in modules and release_major == '9':
            msg += "\n[INFO    ] Please be aware of the following link to ensure that screen sharing within Zoom works as expected: https://videoconference.docs.cern.ch/faqs/#rhel9-and-almalinux9-sharing-screen-functionality"
        logging.info(msg)
        exit(retcode)


def run_input_plugins(config, hostname):
    from pkg_resources import iter_entry_points
    input_plugin_found = 0
    available_methods = []
    for entry_point in iter_entry_points(group='locmap.plugin', name=None):
        try:
            available_methods.append(entry_point.load())
        except ImportError:
            if config.verbose:
                logging.debug("Failed to load entry_point %s" % entry_point)
            continue
        if config.verbose:
            logging.info("Running input plugin : %s" % entry_point)
    # We potentially need 'ip' from the facts plugin inside the xldap
    # plugin. This is why we reverse the list below
    for method in reversed(available_methods):
        if "retrieve_info" in method.__name__:
            input_plugin_found += 1
            method(hostname)


def print_user_help(hostname):
    print("\nNo users have been found by the input plugins.")
    if hostname.fqdn_contains('cern'):
        print("Please use `useraddcern` to add a CERN user. `man useraddcern`")
    else:
        print("Please use `useradd` to add a local user. `man useradd`")


def print_host_information(config, hostname):
    print("--------------\nConfiguration:\n--------------")
    print("    Configuration directory  : " + config.config_dir)
    print("    Configuration file       : " + config.config_file)
    print("    Puppet modules path      : " + config.modules_path)
    print("    Puppet config path       : " + config.puppet_path)
    print("    Puppet hiera path        : " + config.hiera_path)
    print("    Puppet environment       : " + config.env)
    print("    Log directory            : " + config.log_dir)
    print("\n---------\nHostname:\n---------")
    print(hostname)
    print("\n------\nUsers:\n------")
    users = sorted(hostname.users, key=lambda user: user.login)
    if users:
        for user in users:
            print(user)
    else:
        print_user_help(hostname)
    sys.exit(0)


def modify_module(config, module, action, is_aarch64, aarch64_forbidden_modules):
    """
    Manage module status
    """
    is_valid = False
    is_enabled = False
    retcode = 0

    is_enabled = os.path.islink(os.path.join(config.config_dir,
                                             "enabled",
                                             module))
    is_valid = os.path.exists("%s/%s" % (config.modules_path, module))

    target = os.path.join(config.config_dir, "enabled", module)
    source = os.path.join(config.modules_path, module)

    if not is_valid:
        retcode = 3
        return retcode
    if is_aarch64:
        if module in aarch64_forbidden_modules:
            retcode = 5
            return retcode

    if action == "enable" and not is_enabled and is_valid:
        try:
            os.symlink(source, target)
        except Exception as error:
            retcode = 1
            logging.warn("Could not create symlink %s: %s", target, error)
    elif action == "disable" and is_enabled and is_valid:
        try:
            os.remove(target)
        except Exception as error:
            retcode = 1
            logging.warn("Could not delete symlink %s: %s", target, error)
    else:
        retcode = 2

    if (action == "enable" and is_enabled) \
            or (action == "disable" and not is_enabled):
        retcode = 0

    return retcode


def list_modules(config, supporting_module=False,
                 organisation=None, onlyenabled=True):
    """
    Returns the available and enabled puppet modules
    """
    all_modules = set()
    modules = set()

    if not os.path.exists(config.modules_path):
        logging.debug("Puppet module path %s does not exist.",
                      config.modules_path)
        return set()

    all_modules = set([d for d in os.listdir(config.modules_path)
                       if os.path.isdir(os.path.join(config.modules_path, d))])

    for m in all_modules:
        if not supporting_module:
            file = os.path.join(config.modules_path, m, "supporting_module")
            if not os.path.isfile(file):
                modules.add(m)
        else:
            modules.add(m)

    not_organisation = set()
    if organisation is not None:
        for m in modules:
            file = os.path.join(config.modules_path, m, organisation)
            if not os.path.isfile(file):
                not_organisation.add(m)

    not_enabled = set()
    if onlyenabled:
        for m in modules:
            if not os.path.exists(os.path.join(config.config_dir,
                                               'enabled',
                                               m)):
                not_enabled.add(m)

    logging.debug("module list                      : %s" % modules)
    logging.debug("module list without organisation : %s" % not_organisation)
    logging.debug("module list not enabled          : %s" % not_enabled)
    return modules - not_organisation - not_enabled


def print_modules(config, modules):
    print("[Available Modules]")
    template = "{0:15}{1:20}"
    if modules:
        for module in sorted(modules):
            if os.path.exists(config.config_dir + "/enabled/" + module):
                print(template.format(module,
                                      "[\033[92m"+" enabled"+"\033[0m]"))
            else:
                print(template.format(module,
                                      "[\033[91m"+"disabled"+"\033[0m]"))
    else:
        logging.warn("No supported module available")


def configure_module(config, host, modules, is_aarch64, aarch64_forbidden_modules, extra_args=""):
    """
    Run puppet and return execution code
    """
    if config.verbose:
        extra_args = '--test'

    module_list = modules.copy()
    # Handle single module errors case, as multiple module list is curated
    for module in module_list:
        if not os.path.exists("%s/%s" % (config.modules_path, module)):
            modules.remove(module)
            logging.error("Module %s is not installed." % module)
            return 10
        if not os.path.islink(config.config_dir + "/enabled/" + module):
            modules.remove(module)
            logging.error("Module %s is disabled" % module)
            logging.error("Enable it first with `--enable %s`" % module)
            return 11
        if is_aarch64:
            if module in aarch64_forbidden_modules:
                modules.remove(module)
                logging.error("Module %s is not supported on aarch64" % module)
                return 12

    site_pp = os.path.join(config.config_dir, "site.pp")
    hiera_config = os.path.join(config.config_dir, "hiera.yaml")
    yaml_dir = os.path.join(config.hiera_path, config.env, "hieradata")
    yaml_file = os.path.join(yaml_dir, "common.yaml")

    cmd = config.puppet_bin + ' apply \
           %s \
           --hiera_config=%s \
           --color=false \
           --modulepath=%s \
           --environment=%s \
           --detailed-exitcodes \
           %s'
    to_run = cmd % (site_pp, hiera_config, config.modules_path,
                   config.env, extra_args)

    logging.debug('puppet command: %s', to_run)

    for module in module_list:
        imported_module = None
        try:
            imported_module = importlib.import_module(
                'locmap.modules.' + module)
            logging.debug("Imported locmap.modules.%s class", module)
        except ImportError:
            logging.debug("No locmap.modules.%s class", module)

        if imported_module:
            try:
                if imported_module.check_requirements(config, host) != 0:
                    modules.remove(module)
                    logging.debug("Requirement failed. Skipping module %s",
                                  module)
            except:
                logging.debug("No check_requirements function for module %s",
                              module)
            try:
                if imported_module.deploy(config, host) == 0:
                    logging.debug("deployed module %s", module)
                else:
                    modules.remove(module)
                    logging.debug("Deployement failed. Skipping module %s",
                                  module)
            except:
                logging.debug("No deploy function for module %s",
                              module)

    with open(yaml_file, 'w') as common:
        mymodules = {'modules_to_run': list(modules)}
        common.write(yaml.dump(
            mymodules,
            explicit_start=True,
            default_flow_style=False))

    host.write_to_yaml(yaml_file)

    return run_and_show_progressbar(to_run, sleep=0,
                                    msg="[INFO    ]" +
                                    " Please wait while your system" +
                                    " is being configured")


def check_puppet_run_exit_code(retcode):
    """
    Provide extra information about the run via exit codes.
    If --detailed-exitcodes 'puppet apply' will use the following exit codes:

    0: The run succeeded with no changes or failures; the system was already in
    the desired state.

    1: The run failed.

    2: The run succeeded, and some resources were changed.

    4: The run succeeded, and some resources failed.

    6: The run succeeded, and included both changes and failures
    """
    pass


def run_and_show_progressbar(cmd, sleep=0, msg='Please wait '):

    # Adding a progress bar if available
    try:
        from progress.spinner import Spinner
        check_progress = True
    except ImportError:
        check_progress = False
        msg = msg + "..."

    # strip extra space
    cmd = re.sub(' +', ' ', cmd)
    msg = msg + ' '

    if sleep > 0:
        cmd = "/usr/bin/sleep %ds && %s" % (sleep, cmd)

    logging.debug("cmd: `%s`" % cmd)

    try:
        # stdlib can complain, but it's not locmap's fault
        p = subprocess.Popen(cmd,
                             stderr=subprocess.PIPE,
                             shell=True,
                             stdout=subprocess.PIPE,
                             env={'STDLIB_LOG_DEPRECATIONS': 'false'})

    except:
        logging.info("Could not execute %s\n" % cmd)
        return 10

    if check_progress:
        spinner = Spinner(msg)
        while (p.poll() is None):
            spinner.next()
        print('\n')
    else:
        print(msg)

    out, err = p.communicate()
    logging.debug("`%s` returns : %s\n" % (cmd, p.returncode))
    logging.debug("`%s` stdout  :\n%s\n" % (cmd, out.decode('utf-8')))
    logging.debug("`%s` stderr  :\n%s\n" % (cmd, err.decode('utf-8')))
    return p.returncode


def prepare_locmap_config_layout(config):
    retcode = 0
    all_dirs = set()
    # logs = []
    all_dirs.add(config.modules_path)
    return retcode

if __name__ == "__main__":
    main()
