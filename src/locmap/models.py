import socket
import pwd
import sys

from locmap.utils import update_yaml_value, cern_domain_ipv4, cern_domain_ipv6


class Host(object):

    def __init__(self, disallow_root=False):
        self.hostname = socket.gethostname().split('.')[0]
        self.fqdn = self.fqdn()
        self.location = ""
        self.created = ""
        self.modified = ""
        self.managedby = set()
        self.manager = set()
        self.responsibles = set()
        self.mainusers = set()
        self.users = set()  # user object
        self.domain = ""
        self.is_virtual = False
        self.uptime = None
        self.architecture = None
        self.ip = None
        self.ip6 = None
        self.disallow_root = disallow_root

    def unique_logins(self):
        users = set()
        if self.disallow_root:
            return set(self.responsibles)
        if self.manager and not self.mainusers:
            users |= self.manager
        if self.responsibles and not self.managedby:
            users |= self.responsibles
        return set(self.mainusers | self.responsibles | users)

    def mainuser_emails(self):
        addresses = set()
        for user in self.users:
            addresses.add(user.email)
        return addresses

    def mainuser_sshkeys(self):
        keys = set()
        for user in self.users:
            keys |= user.ssh_keys
        return keys

    def unique_mainusers(self):
        users = set()
        if self.manager and not self.mainusers:
            users |= self.manager
        return set(self.mainusers | users)

    def unique_responsibles(self):
        users = set()
        if self.responsibles and not self.managedby:
            users |= self.responsibles
        return set(self.responsibles | users)

    def cern_domain(self):
        domain = "default"
        if self.ip:
            domain = cern_domain_ipv4(self.ip)
        if self.ip6 and 'default' in domain:
            domain = cern_domain_ipv6(self.ip6)
        return domain

    def local_users(self):
        local_users = set()
        local_users.add('root')
        for entry in pwd.getpwall():
            if 'afs' in entry.pw_dir or 'eos' in entry.pw_dir:
                local_users.add(entry.pw_name)
        return local_users

    def add_user(self, login):
        user = User(login)
        self.users.add(user)
        return user

    def fqdn_contains(self, string):
        if string in self.fqdn:
            return True
        else:
            return False

    def fqdn(self):
        full = ""
        if self.hostname:
            full = socket.getfqdn()
        return str(full)

    def __str__(self):
        sb = []
        for key in sorted(self.__dict__):
            if key is not None:
                if not key == "users":
                    sb.append("{key}:'{value}'".format(key=key,
                                                       value=self.__dict__[key]
                                                       ))
        sb.append("cern_domain:%s" % self.cern_domain())
        return '\n'.join(sb)

    def debug(self):
        sb = []
        for key in sorted(self.__dict__):
            sb.append("{key}:'{value}'".format(key=key,
                                               value=self.__dict__[key]
                                               ))
        sb.append("unique_mainusers:%s" % self.unique_mainusers())
        sb.append("mainuser_emails:%s" % self.mainuser_emails())
        sb.append("mainuser_sshkeys:%s" % self.mainuser_sshkeys())
        sb.append("unique_responsibles:%s" % self.unique_responsibles())
        sb.append("local_users:%s" % self.local_users())
        sb.append("cern_domain:%s" % self.cern_domain())
        return ' '.join(sb)

    def write_to_yaml(self, file):
        update_yaml_value(file, "host", str(self.hostname))
        update_yaml_value(file, "host_fqdn", str(self.fqdn))
        update_yaml_value(file, "host_managedby", list(self.managedby))
        update_yaml_value(file, "host_responsibles",
                          list(self.responsibles))
        update_yaml_value(file, "host_mainusers", list(self.mainusers))
        update_yaml_value(file, "host_ipv4", str(self.ip))
        update_yaml_value(file, "host_ipv6", str(self.ip6))
        update_yaml_value(file, "cern_network_domain", self.cern_domain())

    def building(self):
        building = 0
        if '0000 0-0000' not in self.location:
            try:
                import re
                s = re.compile("^(\d+)\s(\d+)-(\d+)").split(self.location)
                building = s[1]
            except:
                pass
        return int(building)

    def building_list(self):
        buildings = set()
        if self.building() != 0:
            buildings.add(self.building())
        return buildings


class User(object):

    def __init__(self, login):
        self.login = ""         # sAMAccountName
        if login:
            self.login = login
        self.name = ""          # displayName
        self.home_dir = ""      # unixHomeDirectory
        self.departement = ""   # division
        self.group = ""         # cernGroup
        self.section = ""       # cernSection
        self.account_type = ""  # cernAccountType
        self.email = ""         # userPrincipalName
        self.office = ""        # physicalDeliveryOfficeName
        self.ssh_keys = set()   # altSecurityIdentities

    # do not define __eq__ on python3
    if (sys.version_info < (3, 0)):
      def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return "%9s\t%11s\t%12s\t%s\t%s" % (self.login,
                                            self.affiliation(),
                                            self.account_type,
                                            self.building(),
                                            self.email)

    def debug(self):
        sb = []
        for key in sorted(self.__dict__):
            sb.append("{key}:'{value}'".format(key=key,
                                               value=self.__dict__[key]
                                               ))
        return ' '.join(sb)

    def affiliation(self):
        return '-'.join([self.departement, self.group, self.section])

    def building(self):
        building = 0
        if '0000 0-0000' not in self.office:
            try:
                import re
                s = re.compile("^(\d+)\s(\d+)-(\d+)").split(self.office)
                building = s[1]
            except:
                pass
        return int(building)
