import os
import locmap.input_plugins.xldapclient
import logging
import sys

def deploy(config, host):
    logging.debug("afs::retrieve_info START")
    try:
        config = locmap.input_plugins.xldapclient.XldapClient()
    except:
        logging.debug("afs::retrieve_info END")
    adduser = '/usr/sbin/useraddcern -s --login %s'
    users = host.unique_logins() - host.local_users()
    for user in users:
        logging.debug("afs::checking if %s has /afs or /eos in their home_dir" % user)
        result = config.user_info(user)
        if not result:
            logging.debug("afs::could not lookup %s" % user)
        else:
            if (sys.version_info > (3, 0)):
                home_dir = result['home_dir'][0].decode('utf-8')
            else:
                home_dir = result['home_dir'][0]
            if '/afs/' in home_dir or '/eos/' in home_dir:
                cmd = adduser % user
                try:
                    os.popen(cmd).read()
                    logging.info("Added user: %s" % user)
                except:
                    logging.error("Failed to add user: %s" % user)
            else:
                logging.info("Ignoring user %s as LDAP home directory is not AFS or EOS" % user)
    return 0
