from locmap.utils import update_yaml_value


def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    path = config.hiera_path + '/production/hieradata/modules/postfix/'
    file = path + 'postfix.yaml'

    update_yaml_value(file, 'local_users', list(host.local_users()))
    update_yaml_value(file, 'mails', list(host.mainuser_emails()))
    return 0
