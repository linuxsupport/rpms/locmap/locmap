from locmap.utils import update_yaml_value


def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    file = config.hiera_path + '/production/hieradata/modules/ssh/ssh.yaml'
    update_yaml_value(file, 'public_keys', list(host.mainuser_sshkeys()))
    return 0
