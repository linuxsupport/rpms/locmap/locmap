from locmap.utils import update_yaml_value

def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    file = config.hiera_path + '/production/hieradata/modules/lpadmin/lpadmin.yaml'
    update_yaml_value(file, 'lpadmin::buildings', list(host.building_list()))
    return 0
