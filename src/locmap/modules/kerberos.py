from locmap.utils import update_yaml_value


def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    file = config.hiera_path + '/production/hieradata/modules/kerberos/kerberos.yaml'
    update_yaml_value(file, 'k5login_values', list(host.unique_logins()))
    return 0
