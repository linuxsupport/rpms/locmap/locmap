from locmap.utils import update_yaml_value

def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    file = config.hiera_path + '/production/hieradata/modules/sudo/sudo.yaml'
    update_yaml_value(file, 'sudo_users', list(host.unique_mainusers()))
    return 0
