from locmap.utils import update_yaml_value


def deploy(config, host):
    """
    Standard locmap model function to include all others functions, methods.
    """
    file = config.hiera_path + '/production/hieradata/modules/chrony/chrony.yaml'
    update_yaml_value(file, 'cern_network_domain',
                      host.cern_domain())
    return 0
