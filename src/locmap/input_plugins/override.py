# -*- coding: utf-8 -*-
import socket
import logging

__author__ = """Thomas Oulevey"""
__email__ = 'thomas.oulevey@cern.ch'
__version__ = '2.0.0'

logging.basicConfig()

def retrieve_info(input):
    result = {}
    logger = logging.getLogger("locmap")
    logger.debug('Input plugin override loaded')

    result = {'coincoin': True}
    return result
