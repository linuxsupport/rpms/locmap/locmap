# -*- coding: utf-8 -*-
import logging
import sys
import locmap.input_plugins.xldapclient

__author__ = """Thomas Oulevey"""
__email__ = 'thomas.oulevey@cern.ch'
__version__ = '2.0.0'


def retrieve_info(hostname):

    logging.debug("xldap::retrieve_info START")
    try:
        config = locmap.input_plugins.xldapclient.XldapClient()
    except:
        logging.debug("xldap::retrieve_info END")
        return 1

    result = config.host_info(hostname)
    if not result:
        logging.debug("xldap::retrieve_info END")
        return 2

    hostname.manager = result.get('mainuser_egroup', '')
    hostname.managedby = result.get('responsible_egroup', '')
    hostname.responsibles = result.get('responsibles', '')
    hostname.mainusers = result.get('mainusers', '')
    hostname.location = result.get('location', '')
    hostname.created = result.get('created', '')
    hostname.modified = result.get('modified', '')

    for login in hostname.unique_logins():
        result = config.user_info(login)
        user = hostname.add_user(login)
        if (sys.version_info > (3, 0)):
          user.login = ''.join(result.get('login')[0].decode('utf-8'))
          user.name = ''.join(result.get('name')[0].decode('utf-8'))
          user.home_dir = ''.join(result.get('home_dir')[0].decode('utf-8'))
          if result.get('departement'):
            user.departement = ''.join(result.get('departement')[0].decode('utf-8'))
          else:
            user.departement = ''
          if result.get('group'):
            user.group = ''.join(result.get('group')[0].decode('utf-8'))
          else:
            user.group = ''
          if result.get('section'):
            user.section = ''.join(result.get('section')[0].decode('utf-8'))
          else:
            user.section = ''
          user.account_type = ''.join(result.get('account_type')[0].decode('utf-8'))
          user.email = ''.join(result.get('email')[0].decode('utf-8'))
          if result.get('office'):
            user.office = ''.join(result.get('office')[0].decode('utf-8'))
          else:
            user.office = ''
        else:
          user.login = ''.join(result.get('login'))
          user.name = ''.join(result.get('name'))
          user.home_dir = ''.join(result.get('home_dir'))
          user.departement = ''.join(result.get('departement'))
          user.group = ''.join(result.get('group'))
          user.section = ''.join(result.get('section'))
          user.account_type = ''.join(result.get('account_type'))
          user.email = ''.join(result.get('email'))
          user.office = ''.join(result.get('office'))
        for key in result.get('ssh_keys'):
            user.ssh_keys.add(key)
        logging.debug(user.debug())

    logging.debug(hostname.debug())
    logging.debug("xldap::retrieve_info END")
    return 0
