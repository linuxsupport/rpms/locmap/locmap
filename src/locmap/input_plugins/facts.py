# -*- coding: utf-8 -*-
import logging
import yaml
import sys
from subprocess import check_output

from locmap.params import DEFAULT_PUPPET_BIN

__author__ = """Thomas Oulevey"""
__email__ = 'thomas.oulevey@cern.ch'
__version__ = '2.0.0'

CMD = "%s facts find --render-as yaml" % DEFAULT_PUPPET_BIN


# Fix for puppet yaml format https://stackoverflow.com/questions/8357650/
def construct_ruby_object(loader, suffix, node):
    return loader.construct_yaml_map(node)


def construct_ruby_sym(loader, node):
    return loader.construct_yaml_str(node)


yaml.add_multi_constructor(u"!ruby/object:", construct_ruby_object)
yaml.add_constructor(u"!ruby/sym", construct_ruby_sym)


def retrieve_info(hostname):

    logging.debug("facts::retrieve_info START")
    try:
        output = check_output(CMD.split())
    except Exception as error:
        logging.debug("facts::retrieve_info END")
        logging.debug("facts::retrieve_info Failed to run command:%s" % CMD)
        return 1

    if not output:
        logging.debug("facts::retrieve_info END")
        logging.debug("facts::retrieve_info Failed with return code 2")
        return 2

    try:
        # yaml will complain on CS9+ if we don't pass Loader
        if (sys.version_info > (3, 9)):
          facts = yaml.load(output, Loader=yaml.FullLoader)
        else:
          facts = yaml.load(output)
    except Exception as error:
        logging.debug("facts::retrieve_info END")
        logging.debug("facts::retrieve_info Could not read yaml %s", error)
        return 3

    try:
        hostname.is_virtual = facts['values']['is_virtual']
    except KeyError:
        logging.debug("facts::retrieve_info is_virtual not found")
    try:
        hostname.uptime = facts['values']['uptime_days']
    except KeyError:
        logging.debug("facts::retrieve_info uptime_days not found")
    try:
        hostname.architecture = facts['values']['architecture']
    except KeyError:
        logging.debug("facts::retrieve_info architecture not found")
    try:
        hostname.domain = facts['values']['domain']
    except KeyError:
        logging.debug("facts::retrieve_info domain not found")
    try:
        hostname.ip = facts['values']['ipaddress']
    except KeyError:
        logging.debug("facts::retrieve_info ipaddress not found")
    try:
        hostname.ip6 = facts['values']['ipaddress6']
    except KeyError:
        logging.debug("facts::retrieve_info ipaddress6 not found")

    logging.debug(facts)
    logging.debug(hostname.debug())
    logging.debug("facts::retrieve_info END")
    return 0
