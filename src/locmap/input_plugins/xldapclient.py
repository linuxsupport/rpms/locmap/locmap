import ldap
import re
import logging
import socket
import sys


class XldapClient:
    """
    XldapCLient class is responsible for interacting with xldap.cern.ch
    """
    def __init__(self):
        """
        Lookup xldap.cern.ch
        """
        self.server = "xldap.cern.ch"
        try:
            ldap.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
            ldap.set_option(ldap.OPT_TIMEOUT, 10)
            self.connection = ldap.initialize('ldap://%s:389' % self.server)
        except ldap.LDAPError as error_message:
            self.connection = None
            logging.verbose('Cannot connect to %s Error: %s',
                            self.server,
                            error_message)

        self.searchScope = ldap.SCOPE_SUBTREE
        self.baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        self.mainusers = []
        self.responsible_users = []
        self.mainuser_egroup = ""
        self.responsibleuser_egroup = ""
        self.location = ""
        self.created = ""
        self.modified = ""

    def connect(self):
        self.conn = ldap.initialize('ldap://%s:389' % self.server)

    def disconnect(self):
        self.conn.unbind()

    def cern_egroups_expansion(self, egroups, conn, processed,
                               recursion=True, fmt="%s"):
        """
        Expands a list of egroups.

        Arguments:
        egroups -- A list of egroups to expand. ['ai-admins', 'it-dep']
        conn -- An initialized LDAP connection object
        processed -- A list of already processed egroups. Pass [].
        recursion -- Boolean to enable recursion
        fmt -- Format string to print usernames. Must have '%s'

        Returns:
        A set of formatted usernames (empty if all egroups are empty)
        """
        base = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch'
        user_regexp = r'CN=(\S+),OU=Users,OU=Organic Units,DC=cern,DC=ch'
        egroup_regexp = r'CN=(\S+),OU=e-groups,OU=Workgroups,DC=cern,DC=ch'

        users = set()
        for egroup in egroups:
            if egroup in processed:
                continue
            processed.append(egroup)
            egroup_filter = "(&(objectClass=group)(CN=%s))" % egroup
            ldap_results = conn.search_s(base, ldap.SCOPE_SUBTREE,
                                         egroup_filter, ['member'])
            for dn, entry in ldap_results:
                if 'member' in entry:
                    for result in entry['member']:
                        if (sys.version_info > (3, 0)):
                          match = re.match(user_regexp, result.decode('utf-8'))
                        else:
                          match = re.match(user_regexp, result)
                        if match is not None:
                            users.add(fmt % match.group(1))
                            continue
                        if recursion is True:
                            if (sys.version_info > (3, 0)):
                              match = re.match(egroup_regexp, result.decode('utf-8'))
                            else:
                              match = re.match(egroup_regexp, result)
                            if match is not None:
                                expanded_egroup = self.cern_egroups_expansion(
                                        [match.group(1)],
                                        conn,
                                        processed,
                                        recursion,
                                        fmt)
                                users = users.union(expanded_egroup)
        return users

    def user_info(self, user):
        """
        Retrieve all attributes from a set of users

        Argument:
            users -- A set of users

        Returns:
            details -- A set of users with details
        """
        logging.debug("checking xldap for user:%s" % user)
        try:
            result_data = None
            baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
            searchFilter = "(&(objectClass=user)(|(cn=%s)))" % user
            retrieveAttributes = ['sAMAccountName',
                                  'displayName',
                                  'unixHomeDirectory',
                                  'division',
                                  'cernGroup',
                                  'cernSection',
                                  'cernAccountType',
                                  'userPrincipalName',
                                  'physicalDeliveryOfficeName',
                                  'altSecurityIdentities']
            result_data = self.connection.search_s(baseDN,
                                                   self.searchScope,
                                                   searchFilter,
                                                   retrieveAttributes)
        except ldap.LDAPError as error_message:
                logging.error('Cannot connect to %s \nError: %s',
                              self.server,
                              error_message)

        if result_data and len(result_data) > 0:
            result_data = result_data[0]
            keys = set()
            identities = result_data[1].get('altSecurityIdentities', '')
            for ssh_key in identities:
                if (sys.version_info > (3, 0)):
                  ssh_key = ssh_key.decode('utf-8')
                if "SSH:" in ssh_key:
                    keys.add(ssh_key)

            login = result_data[1].get('sAMAccountName', '')
            name = result_data[1].get('displayName', '')
            departement = result_data[1].get('division', '')
            section = result_data[1].get('cernSection', '')
            group = result_data[1].get('cernGroup', '')
            home_dir = result_data[1].get('unixHomeDirectory', '')
            account_type = result_data[1].get('cernAccountType', '')
            email = result_data[1].get('userPrincipalName', '')
            office = result_data[1].get('physicalDeliveryOfficeName', '')

            return {'login': login,
                    'name': name,
                    'home_dir': home_dir,
                    'departement': departement,
                    'group': group,
                    'section': section,
                    'account_type': account_type,
                    'email': email,
                    'office': office,
                    'ssh_keys': keys
                    }
        return {'login': user}

    def host_info(self, host):
        """
        Get info from an hostname
        Returns  a set of main users, a set of responsible users,
        and a location as a string.
        """
        responsibles = set()
        mainusers = set()
        mainuser_egroup = set()
        responsible_egroup = set()
        location = ""
        created = ""
        modified = ""

        hostname = host.hostname
        if 'localhost' in hostname:
            try:
                tmphostname = socket.gethostbyaddr(host.ip)[0]
                if 'cern.ch' in tmphostname:
                    hostname = tmphostname[:-8]
            except:
                pass

        try:
            baseDN = "DC=cern,DC=ch"
            retrieveAttributes = ['managedBy',
                                  'manager',
                                  'location',
                                  'whenCreated',
                                  'whenChanged',
                                  'dNSHostName',
                                  'distinguishedName']
            searchFilter = "(&(objectClass=computer)(|(cn=%s)))" % hostname
            result_data = self.connection.search_s(baseDN,
                                                   self.searchScope,
                                                   searchFilter,
                                                   retrieveAttributes)
            if result_data and len(result_data) > 0:
                result_data = result_data[0]
                try:
                    for responsible in result_data[1]['managedBy']:
                        if (sys.version_info > (3, 0)):
                          responsible = responsible.decode('utf-8')
                        user = responsible.split(",")[0]
                        type = responsible.split(",")[1]
                        if user and "e-group" in type:
                            responsible_egroup.add(user.replace('CN=', ''))
                            users = self.cern_egroups_expansion(
                                [user.replace('CN=', ''), ],
                                self.connection,
                                [])
                            responsibles.update(users)
                        else:
                            responsibles.add(user.replace('CN=', ''))
                # If there is no 'managedBy' in Landb/ldap
                except KeyError:
                    pass

                try:
                    for mainuser in result_data[1]['manager']:
                        if (sys.version_info > (3, 0)):
                          mainuser = mainuser.decode('utf-8')
                        user = mainuser.split(",")[0]
                        type = mainuser.split(",")[1]
                        if user and "e-groups" in type:
                            mainuser_egroup.add(user.replace('CN=', ''))
                            users = self.cern_egroups_expansion(
                                [user.replace('CN=', ''), ],
                                self.connection,
                                [])
                            mainusers.update(users)
                        else:
                            mainusers.add(user.replace('CN=', ''))
                # If there is no 'manager' in Landb/ldap
                except KeyError:
                    pass

                if (sys.version_info > (3, 0)):
                  location = ''.join(result_data[1].get('location', '')[0].decode('utf-8'))
                  created = ''.join(result_data[1].get('whenCreated', '')[0].decode('utf-8'))
                  modified = ''.join(result_data[1].get('whenChanged', '')[0].decode('utf-8'))
                else:
                  location = ''.join(result_data[1].get('location', ''))
                  created = ''.join(result_data[1].get('whenCreated', ''))
                  modified = ''.join(result_data[1].get('whenChanged', ''))

            return {'responsibles': responsibles,
                    'mainusers': mainusers,
                    'mainuser_egroup': mainuser_egroup,
                    'responsible_egroup': responsible_egroup,
                    'created': created,
                    'modified': modified,
                    'location': location}

        except ldap.LDAPError as error_message:
            logging.error('Cannot connect to %s.' % self.server)
            logging.debug('Cannot connect to %s. Error: %s',
                          self.server,
                          error_message)
