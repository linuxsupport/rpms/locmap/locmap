import logging

from locmap.params import DEFAULT_LOGGING_LEVEL

def configure_logging(args, default_lvl=DEFAULT_LOGGING_LEVEL):
    """
    Configure application log level

    """
    level = default_lvl
    if args.verbose:
        logging_level = logging.DEBUG
    logging.basicConfig(level=logging_level, format="")
