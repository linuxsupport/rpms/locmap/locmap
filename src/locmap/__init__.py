# -*- coding: utf-8 -*-

"""Top-level package for locmap."""

__author__ = """Thomas Oulevey"""
__email__ = 'thomas.oulevey@cern.ch'
__version__ = '2.0.0'
