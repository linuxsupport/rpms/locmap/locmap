import logging

DEFAULT_LOGGING_LEVEL = logging.INFO
DEFAULT_CONFIG_DIR = '/etc/locmap'
DEFAULT_LOG_DIR = '/var/log'
DEFAULT_LOG_FILE = 'locmap.log'
DEFAULT_CONFIG_FILE = 'locmap.conf'
DEFAULT_MODULES_PATH = '/usr/share/puppet/modules'
DEFAULT_PUPPET_PATH = '/etc/puppetlabs/puppet/'
DEFAULT_HIERA_PATH = '/etc/locmap/code/environments/'
DEFAULT_PUPPET_BIN = '/opt/puppetlabs/bin/puppet'
DEFAULT_PUPPET_SITEPP = 'site.pp'
DEFAULT_HIERADATA_DIR = 'hieradata'
DEFAULT_HIERADATA_COMMON = 'common.yaml'
DEFAULT_ENVIRONMENT = 'production'
DEFAULT_AUTO_RECONFIGURE = 'False'
