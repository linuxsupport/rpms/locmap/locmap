import os
import errno
import yaml
import ctypes
from fnmatch import fnmatch
from netaddr import IPNetwork, IPAddress


def is_user_admin():
    is_admin = False
    try:
        is_admin = os.getuid() == 0
    except AttributeError:
        is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    return is_admin


def merge_two_dicts(x, y):
    """
    Given two dicts, merge them into a new dict as a shallow copy.
    """
    if isinstance(x, dict) and isinstance(y, dict):
        z = x.copy()
        z.update(y)
        return z
    else:
        return None


def update_yaml_value(file, key, value):
    doc = {}
    if not os.path.exists(os.path.dirname(file)):
        try:
            os.makedirs(os.path.dirname(file), mode=0o755)
        except OSError as error:
            if error.errno != errno.EEXIST:
                raise
    if os.path.isfile(file):
        with open(file) as f:
            doc = yaml.safe_load(f)
    if doc is None:
        doc = {}
    doc[key] = value
    with open(file, 'w') as f:
        yaml.safe_dump(doc, f, default_flow_style=False)


def cern_domain_ipv4(ipv4):
    dom = ""
    if ipv4 and fnmatch(ipv4, '*.*.*.*'):
        if fnmatch(ipv4, '10.14[1-9].*') or fnmatch(ipv4, '10.15[0-9].*'):
            dom = 'atlas'
        elif fnmatch(ipv4, '10.17[6-9].*') or fnmatch(ipv4, '10.18[0-9].*') or fnmatch(ipv4, '10.19[0-1].*'):
            dom = 'cms'
        elif fnmatch(ipv4, '137.138.20.24[0-1]') or fnmatch(ipv4, '137.138.20.25[0-5]'):
            dom = 'external'
        elif fnmatch(ipv4, '172.18.*'):
            dom = 'tn'
        elif (
            fnmatch(ipv4, '10.*.*.*') or
            fnmatch(ipv4, '100.6[4-9].*.*') or
            fnmatch(ipv4, '100.[7-9][0-9].*.*') or
            fnmatch(ipv4, '100.1[0-1][0-9].*.*') or
            fnmatch(ipv4, '100.12[0-7].*.*') or
            fnmatch(ipv4, '128.141.*.*') or
            fnmatch(ipv4, '128.142.*.*') or
            fnmatch(ipv4, '137.138.*.*') or
            fnmatch(ipv4, '172.1[6-9].*.*') or
            fnmatch(ipv4, '172.2[0-9].*.*') or
            fnmatch(ipv4, '172.3[0-1].*.*') or
            fnmatch(ipv4, '185.249.5[6-9].*') or
            fnmatch(ipv4, '188.18[4-5].*.*') or
            fnmatch(ipv4, '192.168.*.*') or
            fnmatch(ipv4, '192.65.19[6-7].*') or
            fnmatch(ipv4, '192.91.242.*') or
            fnmatch(ipv4, '194.12.12[8-9].*') or
            fnmatch(ipv4, '194.12.1[3-8][0-9].*') or
            fnmatch(ipv4, '194.12.19[0-1].*')
            ):
            dom = 'others'  
        else:
            dom = 'default'
    return dom


def cern_domain_ipv6(ipv6):
    dom = "default"
    if ipv6 and IPAddress(ipv6).is_unicast() and not IPAddress(ipv6).is_private():
        if IPAddress(ipv6) in IPNetwork('FD01:1458:0200::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0200::/44'):
            dom = 'gpn'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0400::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0400::/44'):
            dom = 'tn'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0600::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0600::/44'):
            dom = 'atlas'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0700::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0700::/44'):
            dom = 'cms'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0300::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0300::/44'):
            dom = 'lcg'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0500::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0500::/44'):
            dom = 'alice'
        elif IPAddress(ipv6) in IPNetwork('FD01:1458:0800::/44') or IPAddress(ipv6) in IPNetwork('2001:1458:0800::/44'):
            dom = 'lhcb'
        elif (
            IPAddress(ipv6) in IPNetwork('2001:1458::/32') or
            IPAddress(ipv6) in IPNetwork('2001:1459::/32') or
            IPAddress(ipv6) in IPNetwork('FD01:1458::/32') or
            IPAddress(ipv6) in IPNetwork('FD01:1459::/32')
            ):
            dom = 'others'   
        else:
            dom = 'default'
    return dom
