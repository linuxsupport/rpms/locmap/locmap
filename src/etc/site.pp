define  locmap::enabled_modules ($module = $title) {
     class {$module :}
       filebucket {$module :
         path => "/etc/locmap/backup/${module}"
       }
       File { backup => $module, }
}
$modules_to_run = lookup('modules_to_run', {'default_value' => []})
locmap::enabled_modules { $modules_to_run: }
