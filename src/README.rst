======
locmap
======


.. image:: https://img.shields.io/pypi/v/locmap.svg
        :target: https://pypi.python.org/pypi/locmap

.. image:: https://img.shields.io/travis/alphacc/locmap.svg
        :target: https://travis-ci.org/alphacc/locmap

.. image:: https://readthedocs.org/projects/locmap/badge/?version=latest
        :target: https://locmap.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/alphacc/locmap/shield.svg
     :target: https://pyup.io/repos/github/alphacc/locmap/
     :alt: Updates


Lo(cal) C(onfiguration) with Ma(sterless) P(uppet)


* Free software: BSD license
* Documentation: https://locmap.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

